"""@file TPdriver.py
   @brief       Direct interface with touch panel
   @details     Initializedthe touch panel and contains methods to
                scan X Y and Z components. Finally can scan all three 
                components
   @author      Tanner Hillman
   @author      Aleya Dolorofino
   @date        12/9/2021
"""

import pyb
import utime
from pyb import ADC
from ulab import numpy as np

class TPdriver:
    ''' @brief Touch Panel Driver
        @details Contains functions of Touch Panel
    '''
    
    def __init__(self, panel, pins):
        ''' @brief Initializes Touch Panel
            @details takes in panel array and pins array to set up touch panel
            
            @param panel List containing all the neccesary values of the Touch Panel
            @param pins List conataining the neccesary pins to read the Touch Panel ADC values
        '''
       
        ## @brief   Xp pin for the system
        # @details  This takes the pins set up in the main code and passes
        #           it into the driver to create the pin
        self.Xp = pins[0]
        
        ## @brief   Yp pin for the system
        # @details  This takes the pins set up in the main code and passes
        #           it into the driver to create the pin
        self.Yp = pins[1]
        
        ## @brief   Xm pin for the system
        # @details  This takes the pins set up in the main code and passes
        #           it into the driver to create the pin
        self.Xm = pins[2]
        
        ## @brief   Ym pin for the system
        # @details  This takes the pins set up in the main code and passes
        #           it into the driver to create the pin
        self.Ym = pins[3]
        
        ## @brief   Touch Panel's X-dimension
        # @details  This takes the Panel Dimensions set up in the main code and passes
        #           it into the driver to help define the Touch Panel
        self.panel_X = panel[0]
        
        ## @brief   Touch Panel's Y-dimension
        # @details  This takes the Panel Dimensions set up in the main code and passes
        #           it into the driver to help define the Touch Panel
        self.panel_Y = panel[1]
        
        ## @brief   Touch Panel's X offset
        # @details  This takes the Panel Dimensions set up in the main code and passes
        #           it into the driver to help define the Touch Panel
        self.panel_X_off = panel[2]
        
        ## @brief   Touch Panel's Y offset
        # @details  This takes the Panel Dimensions set up in the main code and passes
        #           it into the driver to help define the Touch Panel
        self.panel_Y_off = panel[3]
        
        ## @brief   X coordinate of platform center
        # @details  This will be used throughout the code to complete calcs
        #           and balance the ball
        self.Xcenter = self.panel_X / 2 
        
        ## @brief   Y coordinate of platform center
        # @details  This will be used throughout the code to complete calcs
        #           and balance the ball
        self.Ycenter = self.panel_Y / 2
        
    def Xscan(self):
        ''' @brief      Finds X-position of ball on Touch Panel
            @details    Uses X-ADC measurement to determine X-position of the ball.
                        In order for the driver to function properly with the rest
                        of the code the scan must complete in a short period of time
                        so the Xscan also returns time to complete.
            
            @return     both the x-position and time delta measurment
        '''
        
        self.Xp.init(pyb.Pin.OUT_PP) #Configuring Xp to push-pull
        self.Xp.high() #Asserting Xp high
        
        self.Xm.init(pyb.Pin.OUT_PP) #Configuring Xm to push-pull
        self.Xm.low() #Asserting Xm low
        
        self.Yp.init(pyb.Pin.IN) #Configuring Yp to float and setting to input
        
        utime.sleep_us(5)   #After energizing the the resitor divider we wait 
                            #several us to mitigate settling time.
        
        Xscan_time = utime.ticks_us()

        
        self.X_coor = pyb.ADC(self.Ym).read() * (100/4095) - 50   #Finds the x coordinate of the ball
                                                    #using our derived equation        
        
        self.time_change_X = utime.ticks_diff(utime.ticks_us(), Xscan_time)
        
        return self.time_change_X, self.X_coor
        
    def Yscan(self):
        ''' @brief      Finds Y-position of ball on Touch Panel
            @details    Uses Y-ADC measurement to determine Y-position of the ball.
                        In order for the driver to function properly with the rest
                        of the code the scan must complete in a short period of time
                        so the Xscan also returns time to complete.
            
            @return     both the Y-position and time delta measurment
        '''
        
        self.Yp.init(pyb.Pin.OUT_PP) #Configuring Yp to push-pull
        self.Yp.high() #Asserting Yp high
        
        self.Ym.init(pyb.Pin.OUT_PP) #Configuring Ym to push-pull
        self.Ym.low() #Asserting Ym low
        
        self.Xp.init(pyb.Pin.IN) #Configuring Xp to float and setting to input
        
        utime.sleep_us(5)   #After energizing the the resitor divider we wait 
                            #several us to mitigate settling time.
        
        Yscan_time = utime.ticks_us()

        self.Y_coor = pyb.ADC(self.Xm).read() * (176/4095) - 88   #Finds the x coordinate of the ball
                                                    #using our derived equation        
        
        self.time_change_Y = utime.ticks_diff(utime.ticks_us(), Yscan_time)
        
        return self.time_change_Y, self.Y_coor
        
    def Zscan(self):
        ''' @brief      Finds Z-positions of ball on Touch Panel
            @details    Uses X-ADC measurement to determine Z-position of the ball.
                        
            
            @return     a boolean value true if the ball is touching and false if not
        '''
        
        self.Yp.init(pyb.Pin.OUT_PP) #Configuring Yp to push-pull
        self.Yp.high() #Asserting Yp high
        
        self.Xm.init(pyb.Pin.OUT_PP) #Configuring Xm to push-pull
        self.Xm.low() #Asserting Xm low

        utime.sleep_us(5)   #After energizing the the resitor divider we wait 
                            #several us to mitigate settling time.
                            
        #timeZ = utime.ticks_us()
        
        ADCz = pyb.ADC(self.Ym).read()
        
        #deltaZ = utime.ticks_diff(utime.ticks_us(), timeZ)
        
        bool = ADCz < 4000 
        
        #return deltaZ
        return bool
    
    def TPscan(self):
        ''' @brief      Returns a tuple of the x y and z values
            @details    Gets the x and y readings from scanX and scanY, and compiles
                        them in a tuple to reduce computation time
            @return     tuple
        '''
        # returns tuple of x, y, z
        X_tup = self.Xscan()
        Y_tup = self.Yscan()
        Z_val = self.Zscan()
        
        if (Z_val):
            contact = True
            Z_tup = 1
            xyzTupleString = (X_tup, Y_tup, Z_tup)
        else:
            contact = False
       
        if contact:
            return xyzTupleString
        else:
            msg = 'no contact'
            return msg
