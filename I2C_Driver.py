"""@file        I2C_Driver.py      
   @brief       driver for IMU

                   
   @author      Tanner Hillman
   @author      Aleya Dolorofino   
   @date        12/9/2021  
"""

from pyb import I2C
import struct

class I2C_Driver():
    ''' @brief              starts class
    '''
    def __init__ (self, dev_add, i2c):
        '''@brief initializes driver
        
           @param dev_add device address
           @param i2c I2C object
        ''' 
        self.dev_add = dev_add
        self.i2c = i2c
        
        
    def change_mode (self, reg_add):
            '''@brief changes IMU mode
        
               @param reg_add register address
            '''
            self.i2c.mem_write(reg_add,0x28,0x3D)
            
    
    def get_cal_sts (self):
        '''@brief gets calibration status
           @return returns cal status
        '''
       
        buff = bytearray(1)
        cal_sts_bts = self.i2c.mem_read(buff, 0x28, 0x35) 
        
        cal_sts = ( cal_sts_bts[0] & 0b11,
                   (cal_sts_bts[0] & 0b11 << 2) >> 2,
                   (cal_sts_bts[0] & 0b11 << 4) >> 4,
                   (cal_sts_bts[0] & 0b11 << 6) >> 6)
        
        return cal_sts    
    
    def get_cal_cof (self):
            '''@brief gets calibration coefficients
            
               @return returns coefficients
            '''
            byte_buffer = bytearray(22)                    
            self.i2c.mem_write(byte_buffer, self.dev_add, 0x55)
            return tuple(struct.unpack('<hhhhhhhhhhh',byte_buffer))
    
    def snd_cal_cof (self, cof):
        '''@brief sets cal coefficients
            
           @param input coefficients
        '''
        self.i2c.mem_write(cof, self.dev_add, 0x55)
        pass
            
    def get_eul_ang (self):
        '''@brief gets euler angles
           @return returns euler angles
        '''
        self.buffer_eul = bytearray(6)   
        self.i2c.mem_read(self.buffer_eul, 0x28, 0x1a)
        self.euler_angles = struct.unpack('hhh', self.buffer_eul)
        eul_vals = tuple(eul_int/16 for eul_int in self.euler_angles)
        return (eul_vals)
        
    def get_ang_vel (self):
        '''@brief gets angular velocities
           @return returns angular velocities
        '''
        self.buffer_ang = bytearray(6)   
        self.i2c.mem_read(self.buffer_ang, 0x28, 0x14)
        self.ang_velocities = struct.unpack('hhh', self.buffer_ang)
        ang_vals = tuple(eul_int/16 for eul_int in self.ang_velocities)
        return (ang_vals)

