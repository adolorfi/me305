"""@file           motor_driver.py
   @brief          motor driver 

   @author         Tanner Hillman
   @date           11/17/2021
"""


import pyb
#import time

class DRV8847:
      '''@brief                  establishes motor control

      '''
      def __init__ (self, pin_sleep, pin_fault):
          '''@brief      initializes motor control

            
             @param pin_sleep    passes in user input from 
             @param pin_fault    passes in nnn
          '''
          
          self.flag = 'fine'
          self.nSLEEP = pin_sleep
          self.IRQ_src = pyb.ExtInt(pin_fault, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback = self.fault_cb)
          pass
     
      def enable (self):
          '''@brief      enables motor

          '''
          
          self.nSLEEP.high()
          pass
      
      def disable (self):
          '''@brief      disables motor

          '''
          
          self.nSLEEP.low()
          pass
      
      def fault_cb (self, IRQ_src):
          '''@brief      motor fault interrupt

             
             @param IRQ_src fasdf
          '''

          self.nSLEEP.low()
          pass
          self.flag = 'fault'
      
      def update (self):
          ''' @brief              brief goes here

          '''
          if (self.flag == 'fault'):
             msg = 'fault'
             return msg
          elif (self.flag == 'fine'):
             msg = 'fine'
             return msg
         
      def motor (self, outA, pinA, outB, pinB, freq):
          '''@brief      begins to set up motor object
             @details    details go here
             
             @param outA    fasdf
             @param pinA    sdfa
             @param outB    ff
             @param pinB    ff
             @param freq    rr
             @return        returns motor
          '''
          return Motor(outA, pinA, outB, pinB, freq)

class Motor:
    '''@brief      enables motor
       @details    details go here
    '''
    def __init__ (self, outA, pinA, outB, pinB, freq):
        '''@brief      begins to set up motor object
           @details    details go here
             
           @param outA    fasdf
           @param pinA    sdfa
           @param outB    ff
           @param pinB    ff
           @param freq    rr
        '''

        self.tim = pyb.Timer(3, freq = freq)
        self.tchA = self.tim.channel(outA, pyb.Timer.PWM, pin = pinA)
        self.tchB = self.tim.channel(outB, pyb.Timer.PWM, pin = pinB)

        
        pass

    def set_duty(self, duty):
        '''@brief       updates motor
           @details     details go here
           
           @param duty  uhhhh
           @return      asdf
        '''
        
        if duty > 0:
            self.tchA.pulse_width_percent(duty)
        elif duty < 0:
            self.tchB.pulse_width_percent(abs(duty))
        pass

    