"""@file        main.py        
   @brief       Contains all the common variables and runs program
   @details     creates link between all tasks so the neccesary information
                can be shared and specific tasks called so project can
                function as intended
                   
   @author      Tanner Hillman
   @author      Aleya Dolorofino   
   @date        12/9/2021  
"""
import pyb
import task_TP
import time
import task_user
import task_motor
import task_imu
import shares
import task_controller

if __name__ =='__main__':
    ''' @brief begins main code
        @details no details
    '''
    #Inputs for the scanning components
    pinXp = pyb.Pin(pyb.Pin.cpu.A7)
    pinYp = pyb.Pin(pyb.Pin.cpu.A6)
    pinXm = pyb.Pin(pyb.Pin.cpu.A1)
    pinYm = pyb.Pin(pyb.Pin.cpu.A0)
    
    pins = [pinXp, pinYp, pinXm, pinYm]
    
    panelX = 176
    panelY = 100
    offsetX = panelX / 2
    offsetY = panelY / 2
    
    panel = [panelX, panelY, offsetX, offsetY]
    
    #Inputs for the motors
    pin_sleep = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
    pin_fault = pyb.Pin(pyb.Pin.cpu.B2)
    pinA = pyb.Pin(pyb.Pin.cpu.B4)
    pinB = pyb.Pin(pyb.Pin.cpu.B5)
    pinC = pyb.Pin(pyb.Pin.cpu.B1)
    pinD = pyb.Pin(pyb.Pin.cpu.B0)
    freq = 20000


    #Inputs for the shares
    x = shares.Share()
    y = shares.Share()
    z = shares.Share()
    x_vel = shares.Share()
    y_vel = shares.Share()
    z = shares.Share()
    theta_y = shares.Share()
    theta_x = shares.Share()
    theta_y_vel = shares.Share()
    theta_x_vel = shares.Share()
    motor_x_duty = shares.Share()
    motor_y_duty = shares.Share()
    balance = shares.Queue()
    balance.add(False)

    ##controller sets up an object of the task_controller class
    #this will allow us to use the measure values to compute motor
    #torques with the controller
    controller = task_controller.task_controller(balance, z, x, y, x_vel, y_vel, theta_x, theta_y, theta_x_vel, theta_y_vel, motor_x_duty, motor_y_duty)
    
    
    ##TP_task sets up an object of the TPdriver class
    #This will allow us to interact with the touch panel and get
    #position and velocity measurements
    TP_task = task_TP.task_TP(panel, pins, x, y, z, x_vel, y_vel)
    
    ##UserTask sets up an object of the task_user class. This will allow
    #the user to interface with the project.
    UserTask   = task_user.task_user(balance)
    
    ##motor sets up an object of the task_motor class which will allow the 
    #controller to send appropriate torque values to the x and y motor.
    motor = task_motor.task_motor(pin_sleep, pin_fault, pinA, pinB, pinC, pinD, freq, motor_x_duty, motor_y_duty)
    
    ##I2C sets up an object of the task_imu class which will allow the IMU
    #to record angles and angular velocities and send them to the controller
    I2C = task_imu.Task_IMU(theta_y,theta_x,theta_y_vel,theta_x_vel)
  
    ##User Command Interface  
    print (
          'b: begin balancing \n'
          'c: Clear a motor fault condition \n'
          'g: Collect platform data for 30 seconds \n'
          's: End encoder platform data collection prematurely \n'
          'r: Re-print command list \n'
          )

    ##Initializes the serial port so the program can take user inputs
    serport = pyb.USB_VCP()
    
    ##This while loop is the basis for our control scheme. Check for motor 
    #faults > Run IMU to get angles and angular velocities > check touch panel
    #for positions and velocities > send data via shares class to the controller
    #which intreprets data and returns torque values > motors adjust torque 
    #values to balance the ball
    while True:
        try:
            check = motor.update()
            if (check == 'fault'):
                print('There was a motor fault. Press c to clear.')
                motor.stop()
            I2C.run()
            TP_task.run()
            controller.run()
            motor.run()
          
            if serport.any():
                cmd = serport.read(1)
                UserTask.run(cmd)
                continue
            else:
                continue
        except KeyboardInterrupt:
            print('User Interrupt Detected')
            motor.stop()
            break
   
    print('Program Terminating')
    
    
