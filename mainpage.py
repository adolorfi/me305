'''@file                mainpage.py
   @brief               Main page for documentation site.
   @details             This is the main page for the doxygen site. It contains links to 
                        the code for each lab and some information about each one. 

   @mainpage

   @section sec_rep     Repository
                        The repository for all the Labs listed below and their original file version for each lab
                        can be found at https://bitbucket.org/chrisuzuki62/me305_labs

   @section sec_lab0    Lab 0: Fibonacci Sequence
                        Please see Lab_fs.py for details.

   @section sec_lab1    Lab 1: LED
                        The first Nucleo lab, uses a Finite State Machine
                        to implement 3 LED blinking patterns which can be cycled through
                        by pressing a button. Patterns are square, sine, and
                        sawtooth waves.
                        Please see Lab_1.py for details and a demo.


   @section sec_lab2    Lab 2: Encoder Driver and Data Display
                        In this lab we created classes for an encoder driver, and tasks for updating the encoder and taking user input. 
                        These are used to create a program which lets the user
			print current readings for position and speed, as well as collect them over a 30 second period of time.
                        Please see the encoder.py, task_encoder.py, task_user.py files.

   @section sec_lab3	Lab 3: Motor Task and Driver
            			In this lab, we created classes for a motor driver and its corresponding task, then we updated 			
                        the user task to interact with the motor. This will allow for the user to set the duty cycle, 			
                        enable, and disable on both motors. 
                        Please see the DRV8847.py, task_motor.py, and updated task_user.py files.
   @section sec_lab4	Lab 4: Closed Loop Speed Control
            			In this lab, we created classes for a controller driver and its corresponding task, then we updated 			
                        the user task to interact with the controller. This will allow for the user to set the P gain value. 
                        Please see the task_controller.py, Driver_Controller.py, and updated task_user.py files.
   @section sec_lab5	Lab 5: I2C Inertial Measurement Units
            			In this lab, we created classes for a Inertial Sensor 
                        Please see the IMU.py
                        
   @author              Cade Liberty
   @author              Chris Suzuki

   @date                November 17, 2021
'''