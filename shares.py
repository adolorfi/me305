"""@file           shares.py
   @brief          sets up communication
   @details        creates method of communication between variables
    
   @author         Tanner Hillman 
   @author         Aleya Dolorfino
   @date           12/9/2021
"""
class Share:
    '''@brief begins share class
    '''
    
    def __init__(self, value = None):
        '''@brief initializes share class
        
           @param value passes in buffer value
        '''
        
        self.buffer = value
        
    def write(self, item):
        '''@brief writes to buffer
           @param item passes item into buffer
        '''
        
        
        self.buffer = item
        
    def read(self):
        '''@brief reads from buffer
           @return returns the buffer
        '''
        
        return self.buffer
    
class Queue:
    '''@brief begins the queue class
    '''
    
    def __init__(self):
        '''@brief initialized the queue class
        '''
        
        self.buffer = []
        
    def add(self, item):
        '''@breif adds to buffer
           @param item item added to buffer
        '''
        
        
        self.buffer.append(item)
        
    def read(self):
        '''@brief reads from buffer
           @return returns the buffer
        '''
        
        return self.buffer.pop(0)
    
    def num_in(self):
        '''@brief reads length from buffer
           @return returns length of buffer
        '''
        return len(self.buffer)
    