"""@file        task_imu.py      
   @brief       IMU Task

                   
   @author      Tanner Hillman
   @author      Aleya Dolorofino   
   @date        12/9/2021  
"""
import utime
import I2C_Driver
from pyb import I2C

S0_INIT = 0
S1_UPDATE = 1
S2_CALI = 2 

class Task_IMU:
    
    def __init__(self, theta_y, theta_x, theta_y_vel, theta_x_vel):
        ''' @brief              Initializes IMU task
        '''
        self.state = S0_INIT
        self.period = 10000
        
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        self.cal_bool = False
        
        self.theta_y = theta_y
        self.theta_x = theta_x
        self.theta_y_vel = theta_y_vel
        self.theta_x_vel = theta_x_vel
    
    def run(self):
        ''' @brief calibrates IMU and returns values
        '''
       
            if (self.state == S0_INIT):
                #run state 0
                i2c = I2C(1,I2C.MASTER) #sets up i2c as an object of I2C_Driver class
                i2c.init(I2C.MASTER, baudrate = 500000)
                
                dev_add = 0x28
                self.IMU_control = I2C_Driver.I2C_Driver(dev_add, i2c)
    
                reg_add = 0b1100            #register address for 9DOF mode
                self.IMU_control.change_mode(reg_add)
                if self.cal_bool == False:
                    self.state = S2_CALI
                else:
                    self.state = S1_UPDATE
                
                
            if (self.state == S1_UPDATE):
                
                #Reading angles
                ang = self.IMU_control.get_eul_ang()
                self.theta_x.write(ang[1])
                self.theta_y.write(ang[2])
                
                #Reading velocities
                vel = self.IMU_control.get_ang_vel()
                self.theta_x_vel.write(vel[1])
                self.theta_y_vel.write(vel[0])
                
            if (self.state == S2_CALI):
               while (self.cal_bool == False):
                       cal_status = self.IMU_control.get_cal_sts()
                       print(cal_status)
                       if(cal_status[0] + cal_status[1] + cal_status[2] + cal_status[3] == 12):    
                           self.cal_bool == True
                           print('IMU Calibration is Compplete')
                           self.cal_cof = self.IMU_control.get_cal_cof()
                           self.state = S1_UPDATE
                           break
               self.state = S1_UPDATE            
                   
                