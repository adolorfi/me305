"""@file        closedloop.py      
   @brief       closed loop controller

   @author      Tanner Hillman
   @author      Aleya Dolorofino   
   @date        12/9/2021  
"""
from ulab import numpy as np

class ClosedLoop:
    ''' @brief      starts closed loop class
    '''

    
    def __init__(self, K):
        ''' @brief              initializes controller
            @param K            input gain
        '''

        self.K = K
       
    def run(self, state_vector):
        ''' @brief              runs the controller
            @param state_vector input state vector
            @return motor torque
        '''

        motor_torque = np.dot(-self.K, state_vector)
        

        return motor_torque[0]
    
    def get_K(self):
        ''' @brief              gets gain value
            @return             returns k-value
        '''

        return self.K                 
     
         
    def set_K(self, K):
        ''' @brief              sets gain value
            @param              input gain
        '''

        self.K = K