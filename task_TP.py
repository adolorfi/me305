"""@file        task_TP.py      
   @brief       touch panel task
                   
   @author      Tanner Hillman
   @author      Aleya Dolorofino   
   @date        12/9/2021  
"""
import TPdriver
S0_ON = 0
S1_OFF = 1

class task_TP:
    
    def __init__(self, panel, pins, x, y, z, x_vel, y_vel):
        ''' @brief              intializes touch panel task
        '''
        self.state = S0_ON
        self.TP_drv = TPdriver.TPdriver(panel, pins)
        self.TP_state = [0, 0, .0005, .0005, 0, 0]
        self.x = x
        self.y = y
        self.z = z
        self.x_vel = x_vel
        self.y_vel = y_vel
    
    def run(self):
        ''' @brief gets x y positions and velocities
        '''
        if (self.state == S0_ON):
            
            self.Xpos = self.TP_drv.Xscan()
            self.Ypos = self.TP_drv.Yscan()
            self.Xvel = (self.Xpos[1] - self.TP_state[0])/self.TP_state[2]
            self.Yvel = (self.Ypos[1] - self.TP_state[0])/self.TP_state[3]
            self.TP_state = [self.Xpos[1], self.Ypos[1], self.Xpos[0], self.Ypos[0], self.Xvel, self.Yvel]
            self.Zval = self.TP_drv.Zscan()
            
            self.x.write(self.TP_state[0])
            self.y.write(self.TP_state[1])
            self.z.write(self.Zval)
            self.x_vel.write(self.TP_state[4])
            self.y_vel.write(self.TP_state[5])
