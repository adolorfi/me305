"""@file           task_motor.py
   @brief          motor task 

   @author         Tanner Hillman
   @author         Aleya Dolorofino
   @date           11/17/2021
"""


##First Thing in a Task code is to implement the correct modules so the code 
#can function correctly    
#import pyb
#import utime
#import encoder
import motor_driver

class task_motor:
    ''' @brief      motor task class
    '''
    
    def __init__(self, pin_sleep, pin_fault, PinA, PinB, PinC, PinD, freq, motor_x_duty, motor_y_duty):
        ''' @brief          initializes task 
        '''
        self.pin_sleep = pin_sleep
        self.pin_fault = pin_fault
        self.pinA = PinA
        self.pinB = PinB
        self.pinC = PinC
        self.pinD = PinD
        self.freq = freq
        self.motor_x_duty = motor_x_duty
        self.motor_y_duty = motor_y_duty
        
        ##motor_drv sets up an object of the DRV8847 class 
        #this will allow us to control basic functions of the motor driver such 
        #as recognizing motor faults and enabling and disabling the motor's pins
        self.motor_drv = motor_driver.DRV8847(self.pin_sleep, self.pin_fault)
        self.motor_drv.enable()
        ##motor1 and motor2 are both objects of the motor class
        #this will allow us to perform functions direstly related to the motors
        #such as setting the duty cycle
        self.motorx   = self.motor_drv.motor(1, self.pinA, 2, self.pinB, self.freq)
        self.motory   = self.motor_drv.motor(3, self.pinC, 4, self.pinD, self.freq)
        
        
    def update(self):
        ''' @brief      updates the motor
        '''
        self.flag = self.motor_drv.update()
        if (self.flag == 'fault'):
            return self.flag
            
    def run(self):
        ''' @brief      runs the motor based off input

        '''
        print(self.motor_x_duty.read())
        print(self.motor_y_duty.read())
        self.motorx.set_duty(self.motor_x_duty.read()*2)
        self.motory.set_duty(self.motor_y_duty.read()*2)
 
    def stop(self):
        ''' @brief stops the motor
        '''
        self.motor_drv.disable()
        self.motorx.set_duty(0)
        self.motory.set_duty(0)
        print('stoppppp')
        
        
        
        
        