"""@file        task_controller.py      
   @brief       controller task
                   
   @author      Tanner Hillman
   @author      Aleya Dolorofino   
   @date        12/9/2021  
"""
import utime
import closedloop
from ulab import numpy as np

S0_INIT = 0
S1_BALANCE = 1


class task_controller:
    ''' @brief              starts class

    '''
    
    def __init__(self, balance, z, x, y, x_vel, y_vel, theta_x, theta_y, theta_x_vel, theta_y_vel, motor_x_duty, motor_y_duty):
        ''' @brief              intializes task

        '''
        self.state = S0_INIT
        self.period = 10000
        
        self.z = z
        self.x = x
        self.y = y
        self.x_vel = x_vel
        self.y_vel = y_vel
        self.theta_x = theta_x
        self.theta_y = theta_y
        self.theta_x_vel = theta_x_vel
        self.theta_y_vel = theta_y_vel
        self.balance = balance
        self.motor_x_duty = motor_x_duty
        self.motor_y_duty = motor_y_duty
    
    def run(self):
        ''' @brief              run method for controller, creates torque figures

        '''
        if (self.state == S0_INIT):
            #run state 0
            K = np.array([0, -5, 0, -0.2])
            self.cl_x = closedloop.ClosedLoop(K)
            self.cl_y = closedloop.ClosedLoop(K)
            self.motor_x_duty.write(0)
            self.motor_y_duty.write(0)
            if (self.balance.num_in() > 1):
                self.balance.read()
                self.state = S1_BALANCE
            
        if (self.state == S1_BALANCE):
            
            self.stateVector_Mx = np.array([[self.x.read()],
                                             [self.theta_y.read()],
                                             [self.x_vel.read()],
                                             [-self.theta_y_vel.read()]])
            
            self.stateVector_My = np.array([[self.y.read()],
                                             [self.theta_x.read()],
                                             [self.y_vel.read()],
                                             [-self.theta_x_vel.read()]])

            if (self.z.read() == True):
                self.motor_x_duty.write(self.cl_x.run(self.stateVector_Mx))
                self.motor_y_duty.write(self.cl_y.run(self.stateVector_My))
            else:
                self.motor_x_duty.write(0)
                self.motor_y_duty.write(0)
                    
   