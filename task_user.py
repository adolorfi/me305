"""@file           task_user.py
   @brief          user task

   @author         Aleya Dolorofino
   @author         Tanner Hillman
   @date           11/17/2021
"""


##First Thing in a Task code is to implement the correct modules so the code 
#can function correctly    
import pyb


class task_user:
    ''' @brief class started here

    '''
    
    def __init__(self, balance):
        ''' @brief              class initalized

        '''
        self.balance = balance


    
    def cmd(self):
        ''' @brief              if user prompts commands will be displayed

        '''
        msg = (
          'b: begin balancing \n'
          'c: Clear a motor fault condition \n'
          'g: Collect platform data for 30 seconds \n'
          's: End encoder platform data collection prematurely \n'
          'r: Re-print command list \n'
          )
        return msg
    
    def run(self, KeyCommand):
        ''' @brief      the run method 

            
            @return     returns the user input as a string   
        '''
        
        try:
                if (KeyCommand == b'b'):
                    ##The b prefix means 'bytes'.
                    self.balance.add(True)

                
                if (KeyCommand == b'c'):
                    self.out = 'c'
                    return self.out
                
                if (KeyCommand == b'g'):
                    self.out = 'g'
                    return self.out
                
                
                if (KeyCommand == b'r'):
                    self.out = self.cmd()
                    return self.out
                else:
                    key_cmd = 'not valid command'
                    return key_cmd
        
        except:
            key_cmd = 'error'
            return key_cmd
            
            
        
        
        
        
        
        
        
        
        
        